#include <limits.h>
#include <math.h>
#include "gtest/gtest.h"

#include "PlayerTestHarness.h"
#include "Player.h"
#include "PlayerClient.h"
#include "TextureManager.h"
#include "Maths.h"
#include "Colors.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

#include <iostream>
#include <fstream>
#include <thread>

/* Reference: http://www.yolinux.com/TUTORIALS/Cpp-GoogleTest.html */

PlayerTestHarness::PlayerTestHarness()
{
    //nullptr for Player and NPC
  pp = nullptr;
  nn = nullptr;
}

PlayerTestHarness::~PlayerTestHarness()
{
    //resets for Player and NPC
  pp.reset();
  nn.reset();
}

void PlayerTestHarness::SetUp()
{
    GameObject*	go = Player::StaticCreate();
    Player* p = static_cast<Player*>(go);
    this->pp.reset(p);

    GameObject* stop = NPC::StaticCreate();
    NPC* n = static_cast<NPC*>(stop);
    this->nn.reset(n);
}

void PlayerTestHarness::TearDown()
{
    this->pp.reset();
    this->pp = nullptr;

    this->nn.reset();
    this->nn = nullptr;

}

TEST_F(PlayerTestHarness,constructor_noArgs)
{

  //Tests for Player
  EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetColor(), Colors::White));
  EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetLocation(), Vector3::Zero));
  EXPECT_FLOAT_EQ(pp->GetCollisionRadius(), 0.5f);
  EXPECT_FLOAT_EQ(pp->GetScale(),1.0f);
  EXPECT_FLOAT_EQ(pp->GetRotation(),0.0f);
  EXPECT_EQ(pp->GetIndexInWorld(), -1);
  EXPECT_EQ(pp->GetNetworkId(), 0);

  //Tests for NPC
  EXPECT_TRUE(Maths::Is3DVectorEqual(nn->GetColor(), Colors::White));
  EXPECT_TRUE(Maths::Is3DVectorEqual(nn->GetLocation(), Vector3::Zero));
  EXPECT_FLOAT_EQ(nn->GetCollisionRadius(), 0.5f);
  EXPECT_FLOAT_EQ(nn->GetScale(), 1.0f);
  EXPECT_FLOAT_EQ(nn->GetRotation(), 0.0f);
  EXPECT_EQ(nn->GetIndexInWorld(), -1);
  EXPECT_EQ(nn->GetNetworkId(), 0);

  //Tests for Player
  EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetVelocity(), Vector3::Zero));
  EXPECT_EQ(pp->GetPlayerId(), 0.0f);

  //Tests for NPC
  EXPECT_EQ(nn->GetNPCId(), 0.0f);

  //Initial state is update all
  int check = 0x000F; //Hex - binary 00000000 00000000 00000000 00001111
  EXPECT_EQ(pp->GetAllStateMask(), check);

  //Tests for Player
  EXPECT_EQ(pp->GetClassId(), 'PLYR');
  EXPECT_NE(pp->GetClassId(), 'HELP');

  //Tests for NPC
  EXPECT_EQ(nn->GetClassId(), 'NPCS');
  EXPECT_NE(nn->GetClassId(), 'HELP');

  //Added some getters so I could check these - not an easy class to test.
  EXPECT_FLOAT_EQ(pp->GetMaxLinearSpeed(),  50.0f);
  EXPECT_FLOAT_EQ(pp->GetMaxRotationSpeed(), 5.0f);
  EXPECT_FLOAT_EQ(pp->GetWallRestitution(), 0.1f);
  EXPECT_FLOAT_EQ(pp->GetNPCRestitution(), 0.1f);
  EXPECT_FLOAT_EQ(pp->GetLastMoveTimestamp(), 0.0f);
  EXPECT_FLOAT_EQ(pp->GetThrustDir(), 0.0f);
  EXPECT_EQ(pp->GetHealth(), 10);
  EXPECT_FALSE(pp->IsShooting());

  EXPECT_FLOAT_EQ(nn->GetMaxLinearSpeed(), 50.0f);
  EXPECT_FLOAT_EQ(nn->GetMaxRotationSpeed(), 5.0f);
  EXPECT_FLOAT_EQ(nn->GetWallRestitution(), 0.1f);
  EXPECT_FLOAT_EQ(nn->GetNPCRestitution(), 0.1f);
  EXPECT_FLOAT_EQ(nn->GetLastMoveTimestamp(), 0.0f);
  EXPECT_FLOAT_EQ(nn->GetThrustDir(), 0.0f);
  EXPECT_EQ(nn->GetHealth(), 10);
  EXPECT_FALSE(nn->IsShooting());
}

//Test for Player
TEST_F(PlayerTestHarness,EqualsOperator2)
{
  Player *a = static_cast<Player*>(Player::StaticCreate());
  Player *b = static_cast<Player*>(Player::StaticCreate());

  a->SetPlayerId(10);
  b->SetPlayerId(10);

  EXPECT_TRUE(*a == *b);
}

//Test for NPC
TEST_F(PlayerTestHarness, EqualsOperator5)
{
    NPC* a = static_cast<NPC*>(NPC::StaticCreate());
    NPC* b = static_cast<NPC*>(NPC::StaticCreate());

    a->SetNPCId(10);
    b->SetNPCId(10);

    EXPECT_TRUE(*a == *b);
}

//Test for Player
TEST_F(PlayerTestHarness,EqualsOperator3)
{
  Player *a = static_cast<Player*>(Player::StaticCreate());
  Player *b = static_cast<Player*>(Player::StaticCreate());

  a->SetPlayerId(10);
  b->SetPlayerId(30);

  EXPECT_FALSE(*a == *b);
}

//Test for NPC
TEST_F(PlayerTestHarness, EqualsOperator6)
{
    NPC* a = static_cast<NPC*>(NPC::StaticCreate());
    NPC* b = static_cast<NPC*>(NPC::StaticCreate());

    a->SetNPCId(10);
    b->SetNPCId(30);

    EXPECT_FALSE(*a == *b);
}

//Test for Player
TEST_F(PlayerTestHarness,EqualsOperator4)
{
  PlayerPtr b(static_cast<Player*>(Player::StaticCreate()));

  pp->SetPlayerId(10);
  b->SetPlayerId(10);

  EXPECT_TRUE(*pp == *b);
}

//Test for NPC
TEST_F(PlayerTestHarness, EqualsOperator7)
{
    NPCPtr b(static_cast<NPC*>(NPC::StaticCreate()));

    nn->SetNPCId(10);
    b->SetNPCId(10);

    EXPECT_TRUE(*nn == *b);
}

/* Serialistion tests in MemoryBitStreamTestHarness */